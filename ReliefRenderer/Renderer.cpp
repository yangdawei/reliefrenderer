//
//  Renderer.cpp
//  ReliefRenderer
//
//  Created by Dawei Yang on 8/10/14.
//  Copyright (c) 2014 Dawei Yang. All rights reserved.
//
#define GLM_FORCE_RADIANS

#include "Renderer.h"
#include "../common/glm/glm.hpp"
#include "../common/glm/gtc/type_ptr.hpp"
#include "loadshaders.h"

using namespace std;
using namespace glm;

Renderer::~Renderer()
{
    for (auto pObj : objects)
    {
        pObj->release();
        delete pObj;
    }
}

Renderer &Renderer::instance()
{
    static Renderer renderer;
    
    return renderer;
}

void Renderer::renderCallback()
{
    instance().render();
}

void Renderer::reshapeCallback(int width, int height)
{
    instance().reshape(width, height);
}

void Renderer::mouseCallback(int button, int state, int x, int y)
{
    instance().mouse(button, state, x, y);
}

void Renderer::motionCallback(int x, int y)
{
    instance().motion(x, y);
}

void Renderer::reshape(int width, int height)
{
    projMat = perspective<float>(M_PI_2, width / float(height), 1, 20);
    glViewport(0, 0, width, height);
}

void Renderer::mouse(int button, int state, int x, int y)
{
    switch (button)
    {
        case GLUT_LEFT_BUTTON:
            arcball.setWidth(glutGet(GLUT_WINDOW_WIDTH));
            arcball.setHeight(glutGet(GLUT_WINDOW_HEIGHT));
            arcball.saveMat(objects[0]->getModelMat());
            if (state == GLUT_DOWN)
                arcball.start(x, y);
            else
                arcball.stop(x, y);
            break;
            
        default:
            break;
    }
}

void Renderer::motion(int x, int y)
{
    arcball.setWidth(glutGet(GLUT_WINDOW_WIDTH));
    arcball.setHeight(glutGet(GLUT_WINDOW_HEIGHT));
    objects[0]->getModelMat() = arcball.motion(x, y);
    glutPostRedisplay();
}

void Renderer::init()
{
    int argc = 0;
    char argv[1][1];
    glutInit(&argc, (char **) argv);
    glutInitDisplayMode(GLUT_3_2_CORE_PROFILE | GLUT_RGBA | GLUT_DEPTH | GLUT_SINGLE);
    glutCreateWindow("Renderer");
    glutReshapeWindow(1280, 800);
    glutDisplayFunc(Renderer::renderCallback);
    glutReshapeFunc(Renderer::reshapeCallback);
    glutMouseFunc(Renderer::mouseCallback);
    glutMotionFunc(Renderer::motionCallback);
    glEnable(GL_DEPTH_TEST);
//    glEnable(GL_CULL_FACE);
//    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
    
    /*
    ShaderInfo shaders[] = {
        { GL_VERTEX_SHADER, "Shader/triangles.vert" },
        { GL_FRAGMENT_SHADER, "Shader/triangles.frag" },
        { GL_NONE, NULL }
    };
    
    program = LoadShaders(shaders);
    glUseProgram(program);
    */
    
    for (auto ptr : objects)
        ptr->init();
    
}

void Renderer::render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    for (auto ptr : objects)
    {
        GLint location = glGetUniformLocation(ptr->getProgram(), "mModelView");
        glUniformMatrix4fv(location, 1, GL_FALSE, value_ptr(viewMat * ptr->getModelMat()));
        location = glGetUniformLocation(ptr->getProgram(), "mProject");
        glUniformMatrix4fv(location, 1, GL_FALSE, value_ptr(projMat));
        
        ptr->draw();
    }
    
    
    glFlush();
    
    GLenum error = glGetError();
    if (error != GL_NO_ERROR)
    {
        const GLubyte *descrption = glGetString(error);
        printf("error: %s\n", descrption);
        assert(false);
    }
}


void Renderer::startLoop()
{
    glutMainLoop();
}

void Renderer::addObject(string filePath, ObjectType type)
{
    switch (type)
    {
        case TRI_MESH:
            objects.push_back(new TriMesh(filePath));
            break;
        case RELIEF_MESH:
            objects.push_back(new ReliefMesh(filePath));
            break;
        case RELIEF_TEXTURE:
            objects.push_back(new ReliefCube(filePath));
            break;
            
        default:
            assert(false);
    }
}
