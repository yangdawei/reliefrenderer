//
//  ReliefCube.h
//  ReliefRenderer
//
//  Created by Dawei Yang on 8/27/14.
//  Copyright (c) 2014 Dawei Yang. All rights reserved.
//

#ifndef __ReliefRenderer__ReliefCube__
#define __ReliefRenderer__ReliefCube__

#include "ReliefMesh.h"

class ReliefCube : public ReliefMesh
{
public:
    explicit ReliefCube(std::string filename) { setShaderName("reliefCube"); load(filename); }
    
    void init();
    void draw() const;
    
protected:
    void tex2mesh(const ImageVec4f &tex);
    
    float texDepth;
};

#endif /* defined(__ReliefRenderer__ReliefCube__) */
