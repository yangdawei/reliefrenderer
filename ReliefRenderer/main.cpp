//
//  main.cpp
//  ReliefRenderer
//
//  Created by Dawei Yang on 8/10/14.
//  Copyright (c) 2014 Dawei Yang. All rights reserved.
//

#include <iostream>

#include "Renderer.h"
#include "ReliefMesh.h"


int main(int argc, const char *argv[])
{
    Renderer &renderer = Renderer::instance();
    
//    renderer.addObject("../out/1gauss", Renderer::RELIEF_TEXTURE);
//    renderer.addObject("../out/1gauss", Renderer::RELIEF_MESH);
//    renderer.addObject("Model/ARMYGUY.obj", Renderer::TRI_MESH);
//    renderer.addObject("Model/5000_bunny.obj", Renderer::TRI_MESH);
//    renderer.addObject("../out/mulch", Renderer::RELIEF_MESH);
    
    renderer.addObject("../out/mountain", Renderer::RELIEF_MESH);
//    renderer.addObject("../out/mountain", Renderer::RELIEF_TEXTURE);
//    renderer.addObject("Model/ball.obj", Renderer::TRI_MESH);
    
    renderer.init();
    renderer.startLoop();
    
    return 0;
}
