//
//  ReliefMesh.h
//  ReliefRenderer
//
//  Created by Dawei Yang on 8/20/14.
//  Copyright (c) 2014 Dawei Yang. All rights reserved.
//

#ifndef __ReliefRenderer__ReliefMesh__
#define __ReliefRenderer__ReliefMesh__

#include "Mesh.h"
#include "ImageVec4f.h"

class ReliefMesh : public TriMesh
{
public:
    ReliefMesh() {}
    explicit ReliefMesh(std::string filename) { setShaderName("triangles"); load(filename); }
    void load(std::string pfmFile);
    virtual void init();
    virtual void draw() const;
    
protected:
    virtual void tex2mesh(const ImageVec4f &tex);
    void transTexCoords(GLfloat *ptr);
    void computeNormals(const ImageVec4f &tex);
    
    GLfloat *texData;
    GLsizei texWidth, texHeight;
    
    enum tex_ids {TEXTURE, NUM_TEXTURES};
    GLuint textures[NUM_TEXTURES];
};

#endif /* defined(__ReliefRenderer__ReliefMesh__) */
