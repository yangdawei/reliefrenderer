//
//  Mesh.h
//  ReliefRenderer
//
//  Created by Dawei Yang on 8/16/14.
//  Copyright (c) 2014 Dawei Yang. All rights reserved.
//

#ifndef __ReliefRenderer__Mesh__
#define __ReliefRenderer__Mesh__

#define BUFFER_OFFSET(x)  ((const void*) (x))
#define GLM_FORCE_RADIANS

#include <string>
#include <vector>
#include <cmath>

#include <OpenGL/gl3.h>

#include "../common/glm/glm.hpp"

class Mesh
{
public:
    Mesh();
    virtual ~Mesh() = 0;
    virtual void draw() const = 0;
    virtual void load(std::string filename) = 0;
    virtual void init() = 0;
    virtual void release() = 0;
    
    glm::mat4 &getModelMat() { return modelMat; }
    GLuint getProgram() const { return meshProgram; }
    
protected:
    void setShaderName(std::string shaderName) { this->shaderName = shaderName; }
    void initShaders();
    
    GLuint meshProgram;
    
private:
    glm::mat4 modelMat = glm::mat4();
    std::string shaderName;
};
inline Mesh::~Mesh() {}


class TriMesh : public Mesh
{
public:
    TriMesh() { setShaderName("triangles_no_texture"); }
    explicit TriMesh(std::string filename) { setShaderName("triangles_no_texture"); load(filename); }
    
    void draw() const;
    virtual void load(std::string filename);
    void init();
    void release();
    
protected:
    void unitize();
    void transVertex(GLfloat *const ptr);
    void transNormal(GLfloat *const ptr);
    void transIndex(GLuint *const ptr);
    
    template <typename T, typename V>
    void transArray(T *const ptr, std::vector<V> data)
    {
        
    }
    void autogenNormals();
    
    std::vector<GLfloat> vertices;
    std::vector<GLfloat> normals;
    std::vector<GLuint> indices;
//    std::vector<glm::uvec3> normInd;
    enum vao_ids { TRIANGLES, NUM_VAOS };
    enum buffer_ids { ARRAY_BUFFER_V, ARRAY_BUFFER_N, ARRAY_BUFFER_T, ARRAY_BUFFER_I, NUM_BUFFERS };
    enum attrib_ids { V_POSITION, N_POSITION, T_POSITION };
    GLuint vaos[NUM_VAOS];
    GLuint buffers[NUM_BUFFERS];
    
    size_t array_size;
    float sphereRadius;
    glm::vec3 sphereCenter;
};

#endif /* defined(__ReliefRenderer__Mesh__) */
