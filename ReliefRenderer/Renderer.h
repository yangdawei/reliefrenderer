//
//  Renderer.h
//  ReliefRenderer
//
//  Created by Dawei Yang on 8/10/14.
//  Copyright (c) 2014 Dawei Yang. All rights reserved.
//

#ifndef __ReliefRenderer__Renderer__
#define __ReliefRenderer__Renderer__

#define GL_DO_NOT_WARN_IF_MULTI_GL_VERSION_HEADERS_INCLUDED

#include <stdio.h>
#include <string>
#include <vector>
#include <GLUT/GLUT.h>

#include "Mesh.h"
#include "ReliefMesh.h"
#include "ReliefCube.h"
#include "../common/glm/glm.hpp"
#include "../common/glm/gtc/matrix_transform.hpp"
#include "Arcball.h"

class PointLight
{
    glm::vec3 position;
    glm::vec3 color;
};

class Renderer
{
public:
    ~Renderer();
    
    static Renderer &instance();
    
    void init();
    void render();
    void reshape(int width, int height);
    void mouse(int button, int state, int x, int y);
    void motion(int x, int y);
    
    void startLoop();
    
    enum ObjectType {TRI_MESH, RELIEF_MESH, RELIEF_TEXTURE};
    
    void addObject(std::string filePath, ObjectType type);
    
private:
    Renderer() {}
    
    static void renderCallback();
    static void reshapeCallback(int width, int height);
    static void mouseCallback(int button, int state, int x, int y);
    static void motionCallback(int x, int y);
    
    std::vector<Mesh *> objects;
    GLuint program;
    
    glm::mat4 viewMat = glm::lookAt(glm::vec3(0, 0, 2), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0))
    * glm::scale(glm::vec3(2, 2, 2));
    glm::mat4 projMat = glm::perspective<float>(90, 1, 1, 3);
    
    Arcball arcball;
    
    enum buffer_ids { PBO_BUFFER, NUM_BUFFERS };
    GLuint buffers[NUM_BUFFERS];
};

#endif /* defined(__ReliefRenderer__Renderer__) */
