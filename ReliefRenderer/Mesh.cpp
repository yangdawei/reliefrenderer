//
//  Mesh.cpp
//  ReliefRenderer
//
//  Created by Dawei Yang on 8/16/14.
//  Copyright (c) 2014 Dawei Yang. All rights reserved.
//
#define GLM_FORCE_RADIANS

#include <fstream>
#include <iostream>
#include <sstream>
#include <cassert>

#include <OpenGL/gl3.h>

#include "Mesh.h"
#include "loadshaders.h"
#include "../common/glm/glm.hpp"


using namespace std;
using namespace glm;

Mesh::Mesh()
{
}

void Mesh::initShaders()
{
    string sVert = "Shader/" + shaderName + ".vert";
    string sFrag = "Shader/" + shaderName + ".frag";
    ShaderInfo infos[] = {
        { GL_VERTEX_SHADER, sVert.c_str() },
        { GL_FRAGMENT_SHADER, sFrag.c_str() },
        { GL_NONE, NULL }
    };
    meshProgram = LoadShaders(infos);
    glUseProgram(meshProgram);
}

void TriMesh::load(string filename)
{
    vertices.clear();
    
    ifstream input(filename);
    if (input.fail())
    {
        cerr << "Error opening " + filename + ": " << strerror(errno);
        assert(false);
        exit(0);
    }
    
    string header;
    string tmps;
    float x, y, z;
    for (string line; getline(input, line);)
    {
        if (line.size() > 0)
        {
            istringstream is(line);
            is >> header;
            if (header == "v")
            {
                is >> x >> y >> z;
                vertices.push_back(x);
                vertices.push_back(y);
                vertices.push_back(z);
            }
            else if (header == "vn")
            {
                /*
                is >> x >> y >> z;
                normals.push_back(normalize(vec3(x, y, z)));
                 */
                // Do not store normal values, we will generate automatically
            }
            else if (header == "f")
            {
                float tmpvi[3];
                float tmpni[3];
                for (int i = 0; i < 3; i++)
                {
                    is >> tmps;
                    auto p1 = tmps.find_first_of('/');
                    auto p2 = tmps.find_last_of('/');
                    istringstream iv(tmps.substr(0, p1));
                    iv >> tmpvi[i];
                    istringstream in(tmps.substr(p2 + 1, tmps.size() - p2 - 1));
                    in >> tmpni[i];
                }
                for (int k = 0; k < 3; k++)
                    indices.push_back(tmpvi[k] - 1);
            }
            else
            {
            }
        }
    }
    autogenNormals();
    unitize();
}

void TriMesh::draw() const
{
    glBindVertexArray(vaos[TRIANGLES]);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[ARRAY_BUFFER_I]);
    glDrawElements(GL_TRIANGLES, GLsizei(array_size), GL_UNSIGNED_INT, BUFFER_OFFSET(0));
    glBindVertexArray(0);
}

void TriMesh::unitize()
{
    // Compute approximate minimal bounding sphere
    auto length = vertices.size();
    assert(length > 3);
    auto half = length / 2;
    
    vec3 p0 = vec3(vertices[0], vertices[1], vertices[2]);
    vec3 q0 = vec3(vertices[half], vertices[half+1], vertices[half+2]);
    sphereCenter = (p0 + q0) / 2.f;
    sphereRadius = distance(p0, q0) / 2;
    
    for (size_t i = 0; i < vertices.size(); i += 3)
    {
        vec3 s = vec3(vertices[i], vertices[i+1], vertices[i+2]);
        vec3 diff = sphereCenter - s;
        if (glm::length(diff) > sphereRadius)
        {
            diff = normalize(diff);
            vec3 t = diff * sphereRadius + sphereCenter;
            sphereCenter = (s + t) / 2.f;
            sphereRadius = distance(s, t) / 2.f;
        }
    }
    
    for (size_t i = 0; i < vertices.size(); i += 3)
    {
        for (int k = 0; k < 3; k++)
            vertices[i + k] = (vertices[i + k] - sphereCenter[k]) / sphereRadius;
    }
}

void TriMesh::init()
{
    initShaders();
    glGenVertexArrays(NUM_VAOS, vaos);
    glBindVertexArray(vaos[TRIANGLES]);
    
    array_size = 3 * indices.size();
    
    GLfloat *vert_arr = vertices.data();;
    GLfloat *norm_arr = normals.data();
    GLuint *inds_arr = indices.data();
    
    // Buffers
    glGenBuffers(NUM_BUFFERS, buffers);
    
    // Indices
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[ARRAY_BUFFER_I]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * indices.size(), inds_arr, GL_STATIC_DRAW);
    
    // Vertex
    glBindBuffer(GL_ARRAY_BUFFER, buffers[ARRAY_BUFFER_V]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * vertices.size(), vert_arr, GL_STATIC_DRAW);
    glVertexAttribPointer(V_POSITION, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
    glEnableVertexAttribArray(V_POSITION);
    
    // Normal
    glBindBuffer(GL_ARRAY_BUFFER, buffers[ARRAY_BUFFER_N]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * normals.size(), norm_arr, GL_STATIC_DRAW);
    glVertexAttribPointer(N_POSITION, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
    glEnableVertexAttribArray(N_POSITION);

    
    vertices.clear();
    indices.clear();
    normals.clear();
}

void TriMesh::release()
{
    glDeleteVertexArrays(NUM_VAOS, vaos);
    glDeleteBuffers(NUM_BUFFERS, buffers);
}

void TriMesh::transVertex(GLfloat *const ptr)
{
//    transArray<GLfloat, vec3>(ptr, vertices);
}

void TriMesh::transNormal(GLfloat *const ptr)
{
//    transArray<GLfloat, vec3>(ptr, normals);
}

void TriMesh::transIndex(GLuint *const ptr)
{
//    transArray<GLuint, uvec3>(ptr, indices);
}

void TriMesh::autogenNormals()
{
    normals = vertices;
    vector<vector<vec3>> normOfVertices(vertices.size() / 3);
    for (size_t i = 0; i < indices.size(); i += 3)
    {
        GLuint u1 = indices[i + 0];
        vec3 p1 = vec3(vertices[3*u1+0], vertices[3*u1+1], vertices[3*u1+2]);
        GLuint u2 = indices[i + 1];
        vec3 p2 = vec3(vertices[3*u2+0], vertices[3*u2+1], vertices[3*u2+2]);
        GLuint u3 = indices[i + 2];
        vec3 p3 = vec3(vertices[3*u3+0], vertices[3*u3+1], vertices[3*u3+2]);
        vec3 faceNorm = cross(p2 - p1, p3 - p1);
        normOfVertices[indices[i + 0]].push_back(faceNorm);
        normOfVertices[indices[i + 1]].push_back(faceNorm);
        normOfVertices[indices[i + 2]].push_back(faceNorm);
    }
    
    for (size_t i = 0; i < normals.size(); i += 3)
    {
        vec3 sum = vec3(0, 0, 0);
        for (auto j = 0; j < normOfVertices[i / 3].size(); j++)
            sum += normOfVertices[i / 3][j];
        sum = normalize(sum);
        for (int k = 0; k < 3; k++)
            normals[i+k] = sum[k];
    }
}
