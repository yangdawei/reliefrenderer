//
//  Arcball.h
//  ReliefRenderer
//
//  Created by Dawei Yang on 8/19/14.
//  Copyright (c) 2014 Dawei Yang. All rights reserved.
//

#ifndef __ReliefRenderer__Arcball__
#define __ReliefRenderer__Arcball__

#define GLM_FORCE_RADIANS

#include <cstdio>
#include <cmath>

#include "../common/glm/glm.hpp"
#include "../common/glm/gtx/rotate_vector.hpp"

class Arcball
{
public:
    void start(int x, int y)
    {
        status = RUNNING;
        lastVec = calcVec(x, y);
    }
    glm::mat4 motion(int x, int y)
    {
        if (status == STOPPED)
            return glm::mat4();
        
        glm::vec3 curVec = calcVec(x, y);
        glm::mat4 result = glm::rotate(acosf(fmin(glm::dot(curVec, lastVec), 1)), glm::normalize(glm::cross(lastVec, curVec))) * savedMat;
        return result;
    }
    void stop(int x, int y)
    {
        status = STOPPED;
    }
    void setWidth(int w) { width = w; }
    void setHeight(int h) { height = h; }
    void saveMat(const glm::mat4 mat) { savedMat = mat; }
                                      
private:
    glm::vec3 calcVec(int x, int y)
    {
        float fx = x - 0.5 * width;
        float fy = 0.5 * height - y;
        float radius = std::fmax(width, height) * 0.707;
        float tmp = radius * radius - fx * fx - fy * fy;
        if (tmp < 0)
            tmp = 0;
        return glm::normalize(glm::vec3(fx, fy, sqrt(tmp)));
    }
    
    int width;
    int height;
    
    glm::vec3 lastVec;
    glm::mat4 savedMat;
    enum {RUNNING, STOPPED} status;
};

#endif /* defined(__ReliefRenderer__Arcball__) */
