//
//  ReliefMesh.cpp
//  ReliefRenderer
//
//  Created by Dawei Yang on 8/20/14.
//  Copyright (c) 2014 Dawei Yang. All rights reserved.
//
#include <OpenGL/gl3ext.h>

#include "ReliefMesh.h"

using namespace std;
using namespace glm;

void ReliefMesh::init()
{
    TriMesh::init();
    // TexCoord buffer
    GLsizei tex_size = 2 * (texHeight + 1) * (texWidth + 1);
    GLfloat *texcArr = new GLfloat[tex_size];
    transTexCoords(texcArr);
    glBindBuffer(GL_ARRAY_BUFFER, buffers[ARRAY_BUFFER_T]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * tex_size, texcArr, GL_STATIC_DRAW);
    glVertexAttribPointer(T_POSITION, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
    glEnableVertexAttribArray(T_POSITION);
    
    // Texture initialize
    glGenTextures(NUM_TEXTURES, textures);
    glBindTexture(GL_TEXTURE_2D, textures[TEXTURE]);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA32F, texWidth, texHeight);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, texWidth, texHeight, GL_RGBA, GL_FLOAT, texData);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    
    delete[] texcArr;
    delete[] texData;
}

void ReliefMesh::draw() const
{
    glBindTexture(GL_TEXTURE_2D, textures[TEXTURE]);
    
    glBindVertexArray(vaos[TRIANGLES]);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[ARRAY_BUFFER_I]);
    glDrawElements(GL_TRIANGLE_STRIP, GLsizei(array_size), GL_UNSIGNED_INT, BUFFER_OFFSET(0));
    glBindVertexArray(0);
    
    glBindTexture(GL_TEXTURE_2D, 0);
}

void ReliefMesh::load(string pfmFile)
{
    ImageVec4f tex;
    tex.loadPFM(pfmFile);
    
    texWidth = tex.width();
    texHeight = tex.height();
    
    tex2mesh(tex);
    
    texData = new GLfloat[4 * texWidth * texHeight];
    size_t idx = 0;
    for (GLsizei x = 0; x < tex.width(); x++)
        for (GLsizei y = 0; y < tex.height(); y++)
            for (int i = 0; i < 4; i++)
                texData[idx++] = tex.get(x, y)[i];
}

void ReliefMesh::tex2mesh(const ImageVec4f &tex)
{
    int w = tex.width();
    int h = tex.height();
    int w1 = w + 1;
    int h1 = h + 1;
    
    indices.clear();
    vertices.clear();
    normals.clear();
    
    // Compute heights at borderline intersects.
    float *hfield = new float[w1 * h1];
#define CURRENT (hfield[x + y * w1])
    for (int x = 0; x < w1; x++)
    {
        for (int y = 0; y < h1; y++)
        {
            if (x == 0 && y == 0)
                CURRENT = tex.get(x, y)[3];
            else if (x == 0 && y == h)
                CURRENT = tex.get(x, y - 1)[3];
            else if (x == w && y == 0)
                CURRENT = tex.get(x - 1, y)[3];
            else if (x == w && y == h)
                CURRENT = tex.get(x - 1, y - 1)[3];
            else
            {
                if (y == 0)
                    CURRENT = (tex.get(x - 1, y)[3] + tex.get(x, y)[3]) / 2;
                else if (y == h)
                    CURRENT = (tex.get(x - 1, y - 1)[3] + tex.get(x, y - 1)[3]) / 2;
                else if (x == 0)
                    CURRENT = (tex.get(x, y - 1)[3] + tex.get(x, y)[3]) / 2;
                else if (x == w)
                    CURRENT = (tex.get(x - 1, y - 1)[3] + tex.get(x - 1, y)[3]) / 2;
                else
                    CURRENT = (tex.get(x - 1, y - 1)[3] + tex.get(x - 1, y)[3]
                               + tex.get(x, y - 1)[3] + tex.get(x, y)[3]) / 4;
            }
        }
    }
#undef CURRENT
    
    // Add to vertices
    // Borderline
    for (int x = 0; x <= w; x++)
        for (int y = 0; y <= h; y++)
        {
            vertices.push_back(x);
            vertices.push_back(y);
            vertices.push_back(hfield[x + y * w1]);
        }
    
    // Build mesh - triangle strip
    //   0 ___________  w1-1
    //    | /| /| /| /|
    //  w1|/_|/_|/_|/_| 2*w1-1
    //    | /| /| /| /|
    //    |/_|/_|/_|/_|
    
    for (int y = 0; y < h; y++)
    {
        indices.push_back(y * w1);
        for (int x = 0; x < w1; x++)
        {
            indices.push_back(y * w1 + x);
            indices.push_back(y * w1 + x + w1);
        }
        indices.push_back((y + 2) * w1 - 1);
    }
    
    delete[] hfield;
    computeNormals(tex);
    unitize();
}

void ReliefMesh::transTexCoords(GLfloat *ptr)
{
    for (int y = 0; y <= texWidth; y++)
        for (int x = 0; x <= texWidth; x++)
        {
            *ptr = x / float(texWidth);
            ptr++;
            *ptr = y / float(texHeight);
            ptr++;
        }
}

void ReliefMesh::computeNormals(const ImageVec4f &tex)
{
    normals.resize(3 * (texWidth + 1) * (texHeight + 1));
    for (int y = 1; y < texHeight; y++)
        for (int x = 1; x < texWidth; x++)
        {
            vec3 v1 = vec3(x - 1, y - 1, tex.get(x - 1, y - 1)[3]);
            vec3 v2 = vec3(x, y - 1, tex.get(x, y - 1)[3]);
            vec3 v3 = vec3(x, y, tex.get(x, y)[3]);
            vec3 v4 = vec3(x - 1, y, tex.get(x - 1, y)[3]);
            
            vec3 norm = cross(v2 - v4, v1 - v3);
            norm = normalize(norm);
            
            size_t pos = 3 * (y * (texWidth + 1) + x);
            for (int k = 0; k < 3; k++)
                normals[pos + k] = norm[k];
        }
}
