//
//  ReliefCube.cpp
//  ReliefRenderer
//
//  Created by Dawei Yang on 8/27/14.
//  Copyright (c) 2014 Dawei Yang. All rights reserved.
//
#define GLM_FORCE_RADIANS

#include <OpenGL/gl3ext.h>

#include "ReliefCube.h"
#include "loadshaders.h"
#include "../common/glm/gtc/type_ptr.hpp"

using namespace std;
using namespace glm;

void ReliefCube::init()
{
    TriMesh::init();
    
    // Texture initialize
    glGenTextures(NUM_TEXTURES, textures);
    glBindTexture(GL_TEXTURE_2D, textures[TEXTURE]);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA32F, texWidth, texHeight);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, texWidth, texHeight, GL_RGBA, GL_FLOAT, texData);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    
    delete[] texData;
}

void ReliefCube::draw() const
{
    // Set static uniforms.
    GLint location;
    location = glGetUniformLocation(getProgram(), "texWidth");
    glUniform1f(location, texWidth / sphereRadius);
    location = glGetUniformLocation(getProgram(), "texHeight");
    glUniform1f(location, texHeight / sphereRadius);
    location = glGetUniformLocation(getProgram(), "texDepth");
    glUniform1f(location, texDepth / sphereRadius);
    location = glGetUniformLocation(getProgram(), "sphereRadius");
    glUniform1f(location, sphereRadius);
    
    location = glGetUniformLocation(getProgram(), "texCenter");
    glUniform3fv(location, 1, value_ptr(sphereCenter / sphereRadius));
    
    ReliefMesh::draw();
}

/**          z
 *           ^
 *           |
 *           |
 *           |
 *           |
 *          4|__________7
 *          /|         /|
 *         /          / | height field direction: +z
 *       5/__|_______/6 |
 *        | 0 _ _ _ _|__/________> y
 *        | /        | / 3
 *      1 |/_________|/
 *        /         2
 *       /
 *      /
 *     /
 *   |/_
 *    x
 */

void ReliefCube::tex2mesh(const ImageVec4f &tex)
{
    indices.clear();
    vertices.clear();
    normals.clear();
    
    float hmax = numeric_limits<float>().min();
    
    for (unsigned x = 0; x < tex.width(); x++)
        for (unsigned y = 0; y < tex.height(); y++)
        {
            if (hmax < tex.get(x, y)[3])
                hmax = tex.get(x, y)[3];
        }
    texDepth = hmax;
    
    float hmin = 0;
    float xmax = tex.width();
    float ymax = tex.height();
    float xmin = 0;
    float ymin = 0;
    
#define PUSH_3(arr, val1, val2, val3) \
    { (arr).push_back(val1); \
      (arr).push_back(val2); \
      (arr).push_back(val3);}
    PUSH_3(vertices, xmin, ymin, hmin);
    PUSH_3(vertices, xmax, ymin, hmin);
    PUSH_3(vertices, xmax, ymax, hmin);
    PUSH_3(vertices, xmin, ymax, hmin);
    
    PUSH_3(vertices, xmin, ymin, hmax);
    PUSH_3(vertices, xmax, ymin, hmax);
    PUSH_3(vertices, xmax, ymax, hmax);
    PUSH_3(vertices, xmin, ymax, hmax);
    
    PUSH_3(indices, 0, 2, 1);
    PUSH_3(indices, 0, 3, 2);
    PUSH_3(indices, 0, 7, 3);
    PUSH_3(indices, 0, 4, 7);
    PUSH_3(indices, 0, 5, 4);
    PUSH_3(indices, 0, 1, 5);
    
    
    PUSH_3(indices, 6, 7, 4);
    PUSH_3(indices, 6, 4, 5);
    PUSH_3(indices, 6, 5, 1);
    PUSH_3(indices, 6, 1, 2);
    PUSH_3(indices, 6, 2, 3);
    PUSH_3(indices, 6, 3, 7);
    
#undef PUSH_3
    
    unitize();
    autogenNormals();
}
