#version 400 core

smooth in vec3 normal;
smooth in vec3 vertex;

out vec4 fColor;

uniform sampler2D textureMap;
uniform float texWidth;
uniform float texHeight;
uniform float texDepth;
uniform float sphereRadius;
uniform vec3 texCenter;
uniform mat4 mModelView;

const int NONE = -1;
const int POS_X = 0;
const int NEG_X = 1;
const int POS_Y = 2;
const int NEG_Y = 3;
const int POS_Z = 4;
const int NEG_Z = 5;

const float EPS = 1e-4;
const float FLT_MAX = 3.40282347E+38F;
const int LOOP_TIMES = 10;

// Assume that pt is on one of 6 faces.
int onWhichFace(vec3 pt)
{
    vec3 fix = pt + texCenter;
    if (abs(fix.x) < EPS)
        return NEG_X;
    if (abs(fix.y) < EPS)
        return NEG_Y;
    if (abs(fix.z) < EPS)
        return NEG_Z;
    if (abs(fix.x - texWidth) < EPS)
        return POS_X;
    if (abs(fix.y - texHeight) < EPS)
        return POS_Y;
    if (abs(fix.z - texDepth) < EPS)
        return POS_Z;
}

int intersectCube(vec3 source, vec3 direction, out float t)
{
    vec3 fix = source + texCenter;
    vec3 tau;
    if (direction.x > 0)
        tau.x = (texWidth - fix.x) / direction.x;
    else if (direction.x < 0)
        tau.x = -fix.x / direction.x;
    else
        tau.x = FLT_MAX;
    
    if (direction.y > 0)
        tau.y = (texHeight - fix.y) / direction.y;
    else if (direction.y < 0)
        tau.y = -fix.y / direction.y;
    else
        tau.y = FLT_MAX;
    
    if (direction.z > 0)
        tau.z = (texDepth - fix.z) / direction.z;
    else if (direction.z < 0)
        tau.z = -fix.z / direction.z;
    else
        tau.z = FLT_MAX;
    
    t = min(min(tau.x, tau.y), tau.z);
    vec3 intersection = source + direction * t;
    return onWhichFace(intersection);
}

vec4 fetchTexel(vec3 pos)
{
    vec3 fix = pos + texCenter;
    return texture(textureMap, fix.xy / vec2(texWidth, texHeight));
}

vec4 fetchTexelWithIntensity(vec3 pos)
{
    vec3 fix = pos + texCenter;
    vec2 p0 = fix.xy / vec2(texWidth, texHeight);
    vec2 p1 = p0 + vec2(-EPS, -EPS);
    vec2 p2 = p0 + vec2(+EPS, -EPS);
    vec2 p3 = p0 + vec2(+EPS, +EPS);
    vec2 p4 = p0 + vec2(-EPS, +EPS);
    float h0 = texture(textureMap, p0).w / sphereRadius;
    float h1 = texture(textureMap, p1).w / sphereRadius;
    float h2 = texture(textureMap, p2).w / sphereRadius;
    float h3 = texture(textureMap, p3).w / sphereRadius;
    float h4 = texture(textureMap, p4).w / sphereRadius;
    vec3 v1 = vec3(-EPS, -EPS, h1 - h0);
    vec3 v2 = vec3(+EPS, -EPS, h2 - h0);
    vec3 v3 = vec3(+EPS, +EPS, h3 - h0);
    vec3 v4 = vec3(-EPS, +EPS, h4 - h0);
    vec3 tmpNorm = normalize(cross(v1, v2) + cross(v2, v3) + cross(v3, v4) + cross(v4, v1));
//    return texture(textureMap, p0);
//    return vec4(h1, h2, h3, 1);
//    return vec4(tmpNorm / 2 + vec3(.5, .5, .5), 1);
    float intensity = dot(tmpNorm, normalize(vec3(0.5, 0.5, 1)));
    if (intensity < 0)
        intensity = 0;
    return texture(textureMap, p0) * intensity;
}

bool inner(vec3 pos)
{
    vec3 fix = pos + texCenter;
    return texture(textureMap, fix.xy / vec2(texWidth, texHeight)).w >= fix.z * sphereRadius;
}

bool inBoundingBox(vec3 pos)
{
    vec3 fix = pos + texCenter;
    return (fix.x > 0 && fix.x < texWidth && fix.y > 0 && fix.y < texHeight && fix.z > 0 && fix.z < texDepth);
}

void main()
{
    vec4 eyeHomo = inverse(mModelView) * vec4(0, 0, 0, 1);
    vec3 eye = eyeHomo.xyz / eyeHomo.w;

    int faceIn = onWhichFace(vertex);
    
    float tStart, tEnd;
    vec3 base;
    float t;
    vec3 direction = normalize(vertex - eye);
    int faceOut = intersectCube(vertex + EPS * direction, direction, t);
    if (faceIn == faceOut)
        discard;
    
    if (t > 0) // Eye out of bounding box
    {
        tStart = 0, tEnd = t;
        base = vertex + EPS * direction;
    }
    else
    {
        if (faceOut == POS_X)
            fColor = vec4(1, 0, 0, 1);
        if (faceOut == NEG_X)
            fColor = vec4(0, 1, 1, 1);
        if (faceOut == POS_Y)
            fColor = vec4(0, 1, 0, 1);
        if (faceOut == NEG_Y)
            fColor = vec4(1, 0, 1, 1);
        if (faceOut == POS_Z)
            fColor = vec4(0, 0, 1, 1);
        if (faceOut == NEG_Z)
            fColor = vec4(1, 1, 0, 1);
        return;
    }
    
    // Linear search
    if (inner(direction * tStart + base) == inner(direction * tEnd + base))
    {
        bool found = false;
        for (int i = 1; i <= 10; i++)
        {
            tStart = t * (i - 1) / 100;
            tEnd = t * i / 100;
            if (inner(direction * tEnd + base))
            {
                found = true;
                break;
            }
        }
        if (!found) // No intersection
        {
            if (inner(direction * tStart + base))
            {
                fColor = fetchTexelWithIntensity(direction * tStart + base);
                return;
            }
            else
                discard;
        }
    }
    
    // Binary search
    bool bStart = inner(direction * tStart + base);
    
    for (int i = 0; i < LOOP_TIMES; i++)
    {
        float tMid = (tStart + tEnd) / 2;
        if (inner(direction * tMid + base) != bStart)
            tEnd = tMid;
        else
            tStart = tMid;
    }
    float tFinal = (tStart + tEnd) / 2;
    fColor = fetchTexelWithIntensity(direction * tFinal + base);
}
