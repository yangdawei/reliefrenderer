#version 400 core
//#extension GL_ARB_explicit_uniform_location : require

layout(location = 0) in vec4 vPosition;
layout(location = 1) in vec4 vNormal;

uniform mat4 mModelView;
uniform mat4 mProject;
smooth out vec3 vertex;
smooth out vec3 normal;

void main()
{
    vec4 vPosition_ = vPosition;
    vPosition_.w = 1;
    gl_Position = mProject * mModelView * vPosition_;
//    normal = (mModelView * vec4(vNormal.xyz, 0)).xyz;
//    vertex = (mModelView * vec4(vPosition.xyz, 1)).xyz;
    normal = vNormal.xyz;
    vertex = vPosition.xyz;
}