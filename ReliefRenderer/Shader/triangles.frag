#version 400 core

smooth in vec3 normal;
smooth in vec3 vertex;
smooth in vec2 vTexCoord;
out vec4 fColor;

uniform sampler2D textureMap;

void main()
{
    float intensity;
    if (dot(vertex, normal) < 0)
        intensity = dot(normalize(normal), normalize(vec3(0.5, 0.5, 1)));
    else
        intensity = -dot(normalize(normal), normalize(vec3(0.5, 0.5, 1)));

    if (intensity < 0.1)
        intensity = 0.1;
//    fColor = texture(textureMap, vTexCoord);
    fColor = texture(textureMap, vTexCoord) * intensity;
//    fColor = vec4(intensity, intensity, intensity, 1);
//    fColor = vec4(normal, 1);
//    fColor = vec4(1);
}
