//
//  picopng.h
//  ReliefRenderer
//
//  Created by Dawei Yang on 8/30/14.
//  Copyright (c) 2014 Dawei Yang. All rights reserved.
//

#ifndef __ReliefRenderer__picopng__
#define __ReliefRenderer__picopng__

#include <vector>
int decodePNG(std::vector<unsigned char>& out_image, unsigned long& image_width, unsigned long& image_height, const unsigned char* in_png, size_t in_size, bool convert_to_rgba32 = true);
void loadFile(std::vector<unsigned char>& buffer, const std::string& filename); //designed for loading files from hard disk in an std::vector

#endif /* defined(__ReliefRenderer__picopng__) */
