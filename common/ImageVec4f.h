//
//  ImageVec4f.h
//  ReliefRenderer
//
//  Created by Dawei Yang on 8/17/14.
//  Copyright (c) 2014 Dawei Yang. All rights reserved.
//

#ifndef __ReliefRenderer__ImageVec4f__
#define __ReliefRenderer__ImageVec4f__

#include <string>
#include <cassert>
#include <vector>

#include "glm/glm.hpp"

class ImageVec4f
{
protected:
    std::vector<glm::vec4> imageData;
    unsigned w, h;
public:
    const void* getData() const { return imageData.data(); }
    void *getData() { return imageData.data(); }
    ImageVec4f(){}
    ImageVec4f(unsigned w, unsigned h);
    void resize(unsigned w, unsigned h);
    unsigned width() const { return w; }
    unsigned height() const { return h; }
    
    glm::vec4 &get(unsigned x, unsigned y)
    {
        if(!(x<w && y<h))
        {
            assert(false);
            return imageData[(y*w+x) % (w * h)];
        }
        return imageData[y*w+x];
    }
    glm::vec4 get(unsigned x, unsigned y) const
    {
        if(!(x<w && y<h))
        {
            assert(false);
            return imageData[(y*w+x) % (w * h)];
        }
        return imageData[y*w+x];
    }
    void set(unsigned x, unsigned y, glm::vec4 val)
    {
        assert(x < w && y < h);
        imageData[y*w+x] = val;
    }
    void savePFM(const std::string &folderName);
    void loadPFM(const std::string &folderName);
    void loadPNGColor(const std::string &pngFile);
    void loadPNGHeight(const std::string &pngFile, bool normalize = false, float maxHeight = 1.0);
};


#endif /* defined(__ReliefRenderer__ImageVec4f__) */
