//
//  ImageVec4f.cpp
//  ReliefRenderer
//
//  Created by Dawei Yang on 8/17/14.
//  Copyright (c) 2014 Dawei Yang. All rights reserved.
//
#define GLM_FORCE_RADIANS

#include <unistd.h>
#include <sys/stat.h>
#include <iostream>

#include "ImageVec4f.h"
#include "picopng.h"
#include "glm/gtc/type_ptr.hpp"

using namespace std;
using namespace glm;

ImageVec4f::ImageVec4f(unsigned w, unsigned h)
{
    resize(w, h);
}

void ImageVec4f::resize(unsigned w, unsigned h)
{
    this->w = w;
    this->h = h;
    imageData.resize(w*h);
}


void __write(const ImageVec4f &image, string fileName, bool isGray)
{
    FILE *file = fopen(fileName.c_str(), "w");
    if (!file)
    {
        printf("savePFM failed!");
        assert(false);
        return;
    }
    
    unsigned w = image.width();
    unsigned h = image.height();
    
    if (isGray)
        fprintf(file, "Pf\n%d %d\n-1\n", w, h);
    else
        fprintf(file, "PF\n%d %d\n-1\n", w, h);
    fclose(file);
    FILE* file_b = fopen(fileName.c_str(), "ab");
    
    if (isGray)
        for (unsigned i = 0; i < h; i++)
            for (unsigned j = 0; j < w; j++)
                fwrite(&image.get(j, i)[3], sizeof(float), 1, file_b);
    else
        for(unsigned i = 0; i < h; i++)
            for(unsigned j=0; j < w; j++)
                fwrite(value_ptr(image.get(j, i)), sizeof(float), 3, file_b);
    fclose(file_b);
}

void ImageVec4f::savePFM(const std::string &folderName)
{
    __write(*this, folderName + ".texture.pfm", false);
    __write(*this, folderName + ".height.pfm", true);
}

void __read(ImageVec4f &image, string fileName, bool isGray)
{
    FILE *file = fopen(fileName.c_str(), "r");
    if (!file)
    {
        printf("loadPFM failed!");
        assert(false);
        return;
    }
    
    unsigned w, h;
    char buffer[4096];
    fgets(buffer, 4096, file);
    fgets(buffer, 4096, file);
    sscanf(buffer, "%d %d", &w, &h);
    fgets(buffer, 4096, file);
    
    image.resize(w, h);
    
    long pos = ftell(file);
    fclose(file);
    
    FILE *file_b = fopen(fileName.c_str(), "rb");
    fseek(file_b, pos, SEEK_SET);
    if (isGray)
        for (unsigned i = 0; i < h; i++)
            for (unsigned j = 0; j < w; j++)
                fread(&image.get(j, i)[3], sizeof(float), 1, file_b);
    else
        for (unsigned i = 0; i < h; i++)
            for (unsigned j = 0; j < w; j++)
                fread(&image.get(j, i), sizeof(float), 3, file_b);
    fclose(file_b);
}

void ImageVec4f::loadPFM(const std::string &prefix)
{
    __read(*this, prefix + ".texture.pfm", false);
    __read(*this, prefix + ".height.pfm", true);
}

void ImageVec4f::loadPNGColor(const string &pngFile)
{
    vector<unsigned char> buffer, image;
    loadFile(buffer, pngFile);
    unsigned long width, height;
    int error = decodePNG(image, width, height, buffer.empty() ? 0 : &buffer[0], buffer.size());
    if (error != 0)
    {
        cout << error << endl;
        assert(false);
        return;
    }
    
    resize((unsigned int)width, (unsigned int)height);
    size_t idx = 0;
    for (unsigned i = 0; i < height; i++)
        for (unsigned j = 0; j < width; j++)
        {
            vec3 color(image[4 * idx], image[4 * idx + 1], image[4 * idx + 2]);
            color /= 255.f;
            get(j, i)[0] = color[0];
            get(j, i)[1] = color[1];
            get(j, i)[2] = color[2];
            idx++;
        }
}

void ImageVec4f::loadPNGHeight(const string &pngFile, bool normalize /*= false*/, float maxHeight /*= 1.0*/)
{
    vector<unsigned char> buffer, image;
    loadFile(buffer, pngFile);
    unsigned long width, height;
    int error = decodePNG(image, width, height, buffer.empty() ? 0 : &buffer[0], buffer.size());
    if (error != 0)
    {
        cout << error << endl;
        assert(false);
        return;
    }
    
    resize((unsigned int)width, (unsigned int)height);
    size_t idx = 0;
    float fMax = -FLT_MAX;
    float fMin = FLT_MAX;
    for (unsigned i = 0; i < height; i++)
        for (unsigned j = 0; j < width; j++)
        {
            float height = (float(image[4 * idx]) + float(image[4 * idx + 1]) + float(image[4 * idx + 2]));
            get(j, i)[3] = height;
            fMax = std::max(height, fMax);
            fMin = std::min(height, fMin);
            idx++;
        }
    if (normalize)
        for (unsigned i = 0; i < height; i++)
            for (unsigned j = 0; j < width; j++)
            {
                get(j, i)[3] = (get(j, i)[3] - fMin) / (fMax - fMin) * maxHeight;
                idx++;
            }
}
