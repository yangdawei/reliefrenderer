//
//  color.h
//  ReliefRenderer
//
//  Created by Dawei Yang on 9/2/14.
//  Copyright (c) 2014 Dawei Yang. All rights reserved.
//

#ifndef __ReliefRenderer__color__
#define __ReliefRenderer__color__

#include <stdio.h>

typedef struct
{
    double r;       // percent
    double g;       // percent
    double b;       // percent
} rgb;

typedef struct
{
    double h;       // angle in degrees
    double s;       // percent
    double v;       // percent
} hsv;

hsv rgb2hsv(rgb in);
rgb hsv2rgb(hsv in);

#endif /* defined(__ReliefRenderer__color__) */
