//
//  main.cpp
//  TexGenerator
//
//  Created by Dawei Yang on 8/20/14.
//  Copyright (c) 2014 Dawei Yang. All rights reserved.
//

#define GLM_FORCE_RADIANS

#include <iostream>

#include "ImageVec4f.h"
#include "color.h"

using namespace glm;

void addGaussian(ImageVec4f &tex, vec2 center, float radius, float centerHeight)
{
    for (int x = 0; x < tex.width(); x++)
        for (int y = 0; y < tex.height(); y++)
        {
            float tmp = powf(x - center[0], 2) + powf(y - center[1], 2);
            tmp /= 2 * radius * radius;
            tex.get(x, y)[3] += centerHeight * expf(-tmp);
        }
}

void sample1()
{
    const unsigned length = 256;
    ImageVec4f texture(length, length);
    texture.loadPNGColor("images/eclipse256.png");
    
    /*
     for (unsigned i = 0; i < length; i++)
     for (unsigned j = 0; j < length; j++)
     texture.get(i, j) = vec4(0.1, 0.5, 0.5, 0);
     */
    
    addGaussian(texture, vec2(length, length) / 2.f, length / 8, length / 2);
    
    addGaussian(texture, vec2(length, length) / 4.f, length / 16, length / 4);
    addGaussian(texture, vec2(3 * length, length) / 4.f, length / 16, length / 4);
    addGaussian(texture, vec2(length, 3 *length) / 4.f, length / 16, length / 4);
    addGaussian(texture, vec2(3 * length, 3 *length) / 4.f, length / 16, length / 4);
    
    texture.savePFM("../out/1gauss");
}

void sample2()
{
    ImageVec4f texture;
    texture.loadPNGColor("images/mulch-tiled.png");
    texture.loadPNGHeight("images/mulch-heightmap.png");
    texture.savePFM("../out/mulch");
}

void sample3()
{
    ImageVec4f texture;
    texture.loadPNGHeight("images/heightmap.png", true, 50.0);
    float maxHeight = FLT_MIN;
    for (unsigned x = 0; x < texture.width(); x++)
        for (unsigned y = 0; y < texture.height(); y++)
            maxHeight = max(texture.get(x, y)[3], maxHeight);
    
    for (unsigned x = 0; x < texture.width(); x++)
        for (unsigned y = 0; y < texture.height(); y++)
        {
            float height = texture.get(x, y)[3];
            hsv colorHSV;
            colorHSV.h = 180 - height / maxHeight * 180;
            colorHSV.v = colorHSV.s = 0.8;
            rgb colorRGB = hsv2rgb(colorHSV);
            
            texture.set(x, y, vec4(colorRGB.r, colorRGB.g, colorRGB.b, height));
        }
    
    texture.savePFM("../out/mountain");
}

int main(int argc, const char *argv[])
{
    sample3();
    return 0;
}
